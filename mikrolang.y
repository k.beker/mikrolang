    /************************************************
        Remote Homework         2
        header, function, class
        mikrolang.y             <bison grammar file>
        Introduction to Modern Compiler Construction
        05.04.2020
    ************************************************/
%{
    #include <cstdio>
    #include <iostream>
    #include <string>

    // stuff from flex
    extern int yylex();
    extern int yyparse();
    extern FILE *yyin;
    extern int line_num;

    void yyerror(const char *s);
%}

// type tokens
%union {
    int ival;
    float fval;
    char *sval;
}

// oop tokens
%token USING
%token NAMESPACE
%token UNDERSCORE
%token CLASS
%token VOID_KW
%token STRING_KW
%token INT_KW
%token FLOAT_KW
%token COMMA

// costant string tokens
%token WHILE
%token IF ELSE

// boolean logic
%token LESS_OR_EQUAL
%token GREATER_OR_EQUAL
%token EQUAL
%token NOT_EQUAL
%token GREATER_THAN
%token LESS_THAN
%token AND
%token OR

// brackets
%token LEFT_BRACKET RIGHT_BRACKET
%token LEFT_CBRACKET RIGHT_CBRACKET

// maths
%token ADD SUBTRACT
%token MULTIPLY POW MOD DIVIDE
%token SEMICOLON

// counters
%token INCREMENT
%token DECREMENT

// helpers
%token END ENDL
%token ASSIGN

// types
%token <ival> INTEGER
%token <fval> FLOAT
%token <sval> STRING


%%

main: main run
    | run
    ;

run: file_headers ends namespace ends
   | ends

file_headers: file_headers file_header
    | file_header
    ;

file_header: USING name_var SEMICOLON ENDL
    ; 

namespace: NAMESPACE name_var LEFT_CBRACKET ENDL body RIGHT_CBRACKET ENDL
    ;

name_var: STRING UNDERSCORE STRING
    | STRING
    ;

body: CLASS name_var LEFT_CBRACKET ENDL class_body RIGHT_CBRACKET ENDL
    ;

class_body: class_body class_body_content
    | class_body_content
    ;

class_body_content: 
    variable_definition
    | method_definition
    | if_while_definition
    | ENDL
    ;

method_definition:
    VOID_KW name_var LEFT_BRACKET method_parameters RIGHT_BRACKET LEFT_CBRACKET ENDL method_body RIGHT_CBRACKET ENDL
    | allowed_variable_types name_var LEFT_BRACKET method_parameters RIGHT_BRACKET LEFT_CBRACKET ENDL method_body RIGHT_CBRACKET ENDL
    ;

method_parameters: method_parameters COMMA method_parameter
    | method_parameter
    ;

method_parameter:
    allowed_variable_types name_var
    |
    ;

method_body: method_body method_body_content
    | method_body_content
    ;

method_body_content:
    variable_definition
    | if_while_definition
    | counter
    | ENDL
    ;

if_while_definition: 
    IF LEFT_BRACKET boolean_expressions RIGHT_BRACKET LEFT_CBRACKET ENDL in_loop_variables RIGHT_CBRACKET
    | IF LEFT_BRACKET boolean_expressions RIGHT_BRACKET LEFT_CBRACKET ENDL in_loop_variables RIGHT_CBRACKET ELSE LEFT_CBRACKET in_loop_variables RIGHT_CBRACKET
    | WHILE LEFT_BRACKET boolean_expressions RIGHT_BRACKET LEFT_CBRACKET ENDL in_loop_variables RIGHT_CBRACKET
    ;

boolean_expressions: boolean_expression AND boolean_expression
    | boolean_expression OR boolean_expression
    | boolean_expression

boolean_expression: expression GREATER_THAN expression
    | expression NOT_EQUAL expression
    | expression LESS_THAN expression
    | expression EQUAL expression
    | expression LESS_OR_EQUAL expression
    | expression GREATER_OR_EQUAL expression

in_loop_variables: in_loop_variables variable_definition
    | in_loop_variables counter
    | variable_definition
    | counter
    ;

counter: name_var INCREMENT SEMICOLON ENDL
    | name_var DECREMENT SEMICOLON ENDL

variable_definition:
    allowed_variable_types name_var ASSIGN statement SEMICOLON ENDL
    | allowed_variable_types name_var ASSIGN mult_expr SEMICOLON ENDL 
    ;

allowed_variable_types:
    STRING_KW
    | INT_KW
    | FLOAT_KW
    ;

statement:
    | STRING
    | INTEGER
    | FLOAT
    ;

mult_expr: mult_expr expression
    | expression
    ;

expression: FLOAT
    | STRING
    | INTEGER
    | expression ADD expression
    | expression SUBTRACT expression
    | expression DIVIDE expression
    | expression MULTIPLY expression
    | expression POW expression
    | expression MOD expression

ends: ends end
    | end
    ;

end:
    END ENDL                                    { YYABORT; }
    | END                                       { YYABORT; }
    | ENDL                                    
    ;

%%

std::ostream& bold_on(std::ostream& os) {
    return os << "\e[1m";
}

std::ostream& bold_off(std::ostream& os) {
    return os << "\e[0m";
}

std::ostream& error(std::ostream& os) {
    return os << "\e[31m";
}

std::ostream& info(std::ostream& os) {
    return os << "\e[32m";
}

// main c++ function
int main(int argc, char** argv) {
    if (argc > 1) {
        FILE *srcfile = fopen(argv[1], "r");
        // test validity
        if (!srcfile) {
            std::cout << bold_on << error << "[PANIC] can't open " << bold_off << argv[1] << std::endl;
            exit(-1);
        }
        std::cout << bold_on << info << "[INFO] reading source file from: " << bold_off << argv[1] << std::endl;
        // set flex to read from file
        yyin = srcfile;
        // parse input
        yyparse();

        std::cout << bold_on << info << "[INFO] sucessfuly parsed file from: " << bold_off << argv[1] << std::endl;
        return 0;
    }
    std::cout << bold_on << info << "[INFO] reading from STDIN " << bold_off << std::endl;
    // parse directly from stdin
    yyparse();
    return 0;
}

void yyerror(const char *s) {
    std::cout << bold_on << error << "[PANIC]" << "\n" << "LINE: "<< line_num << "\n" << "MESSAGE: " << bold_off << s << std::endl;
    // halt the program
    exit(-1);
}

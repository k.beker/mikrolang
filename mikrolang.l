    /************************************************
        Remote Homework     2
        namespace, function, class
        mikrolang.l         <flex file>
        Introduction to Modern Compiler Construction
        05.04.2020
    ************************************************/
%{
    #include "mikrolang.tab.h"
    int line_num = 1;
%}
%option noyywrap
%option yylineno
string [a-zA-Z0-9]
digit [0-9]
%%
    /* Exit on EOF */
<<EOF>>             { yyterminate(); return 0; }

    /* Handle oop grammars */
"using"             { return USING; }
"namespace"         { return NAMESPACE; }
"class"             { return CLASS; }

    /* Keyword types */
"void"              { return VOID_KW; }
"str"               { return STRING_KW; }
"int"               { return INT_KW; }
"float"             { return FLOAT_KW; }

    /* Additional charcters */ 
"_"                 { return UNDERSCORE; }
","                 { return COMMA; }

    /* Ignore whitespaces, newlines and dots */
[ \t]               ;
\.                  ;

    /* Handle new lines and end keyword */
\n                  { ++line_num; return ENDL; }
"end"               { return END; }

    /* While If Else statements */
"while"			    { return WHILE; }
"if"				{ return IF; }
"else"			    { return ELSE; }

    /* Logical Operators */
"<="                { return LESS_OR_EQUAL; }
">="                { return GREATER_OR_EQUAL; }
"=="                { return EQUAL; }
"!="                { return NOT_EQUAL; }
">"                 { return GREATER_THAN;}
"<"                 { return LESS_THAN;}
"&&"                { return AND;}
"||"                { return OR;}

    /* Math Operators */
"="                 { return ASSIGN; }
"("                 { return LEFT_BRACKET; }
")"                 { return RIGHT_BRACKET; }
"{"                 { return LEFT_CBRACKET; }
"}"                 { return RIGHT_CBRACKET; }
"+"                 { return ADD; }
"-"                 { return SUBTRACT; }
"*"                 { return MULTIPLY; }
"**"                { return POW; }
"%"                 { return MOD; }
"/"                 { return DIVIDE; }
";"                 { return SEMICOLON; }

    /* Increment Decrement */
"--"                { return DECREMENT; }
"++"                { return INCREMENT; }

    /* Handle Types */
{digit}+		    { yylval.fval = atof(yytext); return INTEGER; }
{digit}+\.{digit}   { yylval.fval = atoi(yytext); return FLOAT; }
{string}+		    {
    yylval.sval = strdup(yytext);
    return STRING;
}

%%

all: mikrolang

mikrolang.tab.c mikrolang.tab.h:	mikrolang.y
		bison -d mikrolang.y

lex.yy.c: mikrolang.l mikrolang.tab.h
		flex mikrolang.l

mikrolang: lex.yy.c mikrolang.tab.c mikrolang.tab.h
		g++ -o mikrolang mikrolang.tab.c lex.yy.c

clean:
		rm mikrolang mikrolang.tab.c lex.yy.c mikrolang.tab.h

test:
		bash -c "./mikrolang < input.working"
		bash -c "./mikrolang input.working"
